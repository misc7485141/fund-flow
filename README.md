## Homework

### How to set up things

```
composer install
php requirements.php                    // TODO: Should be repurposed for this app later.
./init                                  // TODO: Pretty much only sets debug and env consts for now. Requires reconstruction.
./yii migrate
./vendor/bin/codecept build
./vendor/bin/codecept run
./yii main/import-currencies            // You could use these for seeding prod db aswell as dev db.
./yii exchange-rates/launch-updater     // Starts updating exhange rates periodically.
```

### Exchange rate updater

You could use launcher.sh for updating exchange rates. Although it is not very sophisticated. Set up cron job like this:
```
* * * * * /usr/bin/bash /realpath/homework/misc/launcher.sh /realpath/homework/yii exchange-rates/launch-updater &> /dev/null
```
It will make sure that only one process is running and will restart it in case it died.
Using Jenkins or something else is preferable.

### Checking requirements

Hopefully I can finish this in time. Only default Yii2 checks are ready at the moment, some of them are not even mandatory.

### Environments

There is infrastructure ready for supporting different configs. As of this moment only one set is hardcoded.\
But you can refactor a bit, create different varieties and put them in respective env folders for init command.

### Final notes

There are still things to do but my time is running low. Critical unit tests are more or less completed, although edge case testing might be better.\
Idea was to shift to functional and acceptance testing after that. And exchange rate module tests are also missing.\
\
Speaking about that module, updater and 3rd party service lack polish, might cause trouble.\
\
App configs are messy, would be great to have extra time to structure them for easier maintainability.\
I hope that main idea is clear behind those. Should be easy to refactor.
