<?php

defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('YII_ENV') or define('YII_ENV', 'prod');

$vendorPath = dirname(__DIR__, 2) . '/vendor';
$configPath = dirname(__DIR__) . '/config';

require "$vendorPath/autoload.php";
require "$vendorPath/yiisoft/yii2/Yii.php";
require "$configPath/bootstrap.php";

use yii\web\Application;

$config = (require "$configPath/configApp.php");

(new Application($config))->run();
