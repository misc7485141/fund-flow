<?php

return [
	'Development' => [
		'path' => 'dev',
		'setWritable' => [
			'app/runtime'
		],
		'setExecutable' => [
			'yii'
		]
	],
	'Production' => [
		'path' => 'prod',
		'setWritable' => [
			'app/runtime'
		],
		'setExecutable' => [
			'yii'
		]
	]
];
