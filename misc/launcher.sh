#!/bin/bash

program="$(realpath -e "$1")"
action="$2"
lockDir="/tmp/$(basename -s '.php' "$program")-$(echo "$action" | sed -e 's/\//-/g').lock"

if mkdir "$lockDir" 2> /dev/null; then
	trap 'kill $(cat "$lockDir/pid"); rm -rf "$lockDir"' 0
	trap 'exit 2' 1 2 3 15
else
	exit 0
fi

php "$program" "$action" &
pid="$!"
echo "$pid" > "$lockDir/pid"
wait "$pid"
