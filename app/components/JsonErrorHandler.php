<?php

namespace app\components;

use yii\web\{ErrorHandler, Response};
use Yii;

class JsonErrorHandler extends ErrorHandler
{
	/**
	 * @inheritDoc
	 */
	protected function renderException($exception): void
	{
		Yii::$app->getResponse()->format = Response::FORMAT_JSON;
		parent::renderException($exception);
	}
}
