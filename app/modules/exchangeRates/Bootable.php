<?php

namespace exchangeRates;

use yii\base\Module;

final class Bootable extends Module
{
	public $controllerNamespace = 'exchangeRates\commands';
}
