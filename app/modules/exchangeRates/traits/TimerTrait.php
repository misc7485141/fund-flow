<?php

namespace exchangeRates\traits;

trait TimerTrait
{
	private int $lastTime = 0;
	private int $interval = 0;

	private function IsTime(): bool
	{
		$currentTime = time();
		if (!$this->lastTime || (($currentTime - $this->lastTime) >= $this->interval)) {
			$this->lastTime = $currentTime;
			return true;
		}
		return false;
	}

	private function ResetLastTime(): void
	{
		$this->lastTime = 0;
	}
}
