<?php

namespace exchangeRates\services;

use app\models\{CurrencyRecord, ExchangeRateRecord};
use exchangeRates\interfaces\ProviderInterface;
use exchangeRates\traits\TimerTrait;
use yii\helpers\ArrayHelper;

class UpdaterService
{
	const RELOAD_CURRENCIES_INTERVAL = (60 * 5);

	/** @var ProviderInterface[] */
	private array $providers;
	private array $currencyIdsByAlphabeticCodes = [];

	use TimerTrait;

	/**
	 * @param ProviderInterface[] $providers
	 */
	public function __construct(array $providers, int $interval = self::RELOAD_CURRENCIES_INTERVAL)
	{
		$this->providers = $providers;
		$this->interval = $interval;
	}

	public function Iterate(): void
	{
		if ($this->IsTime()) {
			$this->currencyIdsByAlphabeticCodes =
				ArrayHelper::map(CurrencyRecord::find()->all(), 'alphabetic_code', 'id');
		}

		foreach ($this->currencyIdsByAlphabeticCodes as $alphabeticCode => $currencyId) {
			if ($this->HasDatedOrMissingOutgoingExchangeRate($currencyId)) {
				// We could update only expired ones but bulk updating is cheaper, fewer requests to 3rd party apis.
				$expired = $this->currencyIdsByAlphabeticCodes;

				// Update self to self rate.
				ExchangeRateRecord::UpdateExchangeRate($currencyId, $currencyId, 1);
				unset($expired[$alphabeticCode]);
				if (empty($expired)) continue;

				// TODO: Rethink this because all rates will be nullified if connection is lost.

				// Update other rates using all available providers if necessary.
				foreach ($this->providers as $provider) {
					$alphabeticCodes = array_keys($expired);
					foreach ($provider::GetExchangeRates($alphabeticCode, $alphabeticCodes) as $code => $rate) {
						ExchangeRateRecord::UpdateExchangeRate($currencyId, $expired[$code], $rate);
						unset($expired[$code]);
					}
					if (empty($expired)) break;
				}

				// There is no info about these, unset rates and handle errors in apis.
				foreach ($expired as $id) {
					ExchangeRateRecord::UpdateExchangeRate($currencyId, $id, null);
				}
			}
		}
	}

	private function HasDatedOrMissingOutgoingExchangeRate(int $currencyId): bool
	{
		$result = ExchangeRateRecord::find()->select([
			'check1' => 'IFNULL((MIN(update_time) < (NOW() - INTERVAL ' . ExchangeRateRecord::DATED_IN . ' SECOND)), 1)',
			'check2' => 'COUNT(*)',
		])->where(['currency_from' => $currencyId])->asArray()->one();

		return ($result['check1'] || ($result['check2'] < count($this->currencyIdsByAlphabeticCodes)));
	}
}
