<?php

namespace exchangeRates\providers;

use exchangeRates\interfaces\ProviderInterface;
use Yii;

class ExchangeRatesDataAPI implements ProviderInterface
{
	const API = 'https://api.apilayer.com/exchangerates_data';

	// TODO: Implement error parsing and stuff because this might break any minute.
	public static function GetExchangeRates(string $currencyFrom, array $currenciesTo): array
	{
		$symbols = implode(',', $currenciesTo);

		$options = [
			CURLOPT_URL => (self::API . "/latest?symbols=$symbols&base=$currencyFrom"),
			CURLOPT_HTTPHEADER => [
				'Content-Type: text/plain',
				'apikey: ' . Yii::$app->params['exchangeRatesDataAPIKey']
			],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET'
		];
		if (YII_ENV_DEV) {
			$options[CURLOPT_SSL_VERIFYPEER] = false;
		}

		$curl = curl_init();
		curl_setopt_array($curl, $options);
		$response = curl_exec($curl);
		curl_close($curl);

		return (json_decode($response, true)['rates'] ?? []);
	}
}
