<?php

namespace exchangeRates\interfaces;

interface ProviderInterface
{
	public static function GetExchangeRates(string $currencyFrom, array $currenciesTo): array;
}
