<?php

namespace app\resources;

class Strings
{
	const ERROR_CURRENCY_DESTINATION_ACCOUNT_MISMATCH = 'currency_destination_account_mismatch';
	const ERROR_UNAVAILABLE_OR_EXPIRED_EXCHANGE_RATE = 'unavailable_or_expired_exchange_rate';
	const ERROR_INSUFFICIENT_FUNDS = 'insufficient_funds';
	const ERROR_TRANSACTION_FAILED = 'transaction_failed';
	const ERROR_INVALID_ACCOUNT_ID = 'invalid_account_id';
	const ERROR_UNEXPECTED_ARGUMENT = 'unexpected_argument';
	const ERROR_NOT_ENOUGH_FUNDS_IN_ACCOUNT = 'not_enough_funds_in_account';
	const ERROR_SAME_ACCOUNT_TRANSFER = 'same_account_transfer';
}
