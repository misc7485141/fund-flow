<?php

namespace app\controllers;

use app\presenters\AccountsPresenter;
use app\models\AccountRecord;
use yii\filters\VerbFilter;
use yii\web\Controller;

class AccountsController extends Controller
{
	public $enableCsrfValidation = false;

	public function behaviors(): array
	{
		return [
			'verbs' => [
				'class' => VerbFilter::class,
				'actions' => [
					'get-client-accounts' => ['GET']
				]
			]
		];
	}

	public function actionGetClientAccounts(int $clientId): void
	{
		$query = AccountRecord::find()
			->where(['client_id' => $clientId])->with('currency');

		$this->asJson([
			'accounts' => AccountsPresenter::FormatData($query->all()),
			'meta' => [
				'total' => (int)$query->count()
			]
		]);
	}
}
