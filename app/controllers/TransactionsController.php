<?php

namespace app\controllers;

use app\models\{AccountRecord, TransactionHistoryRecord};
use yii\base\{DynamicModel, UserException};
use app\presenters\TransactionsPresenter;
use app\services\TransactionService;
use yii\filters\VerbFilter;
use app\resources\Strings;
use yii\web\Controller;
use Exception, Yii;

class TransactionsController extends Controller
{
	public $enableCsrfValidation = false;

	public function behaviors(): array
	{
		return [
			'verbs' => [
				'class' => VerbFilter::class,
				'actions' => [
					'get-account-history' => ['GET'],
					'transfer-funds' => ['POST']
				]
			]
		];
	}

	/**
	 * @throws UserException
	 */
	public function actionGetAccountHistory(int $accountId, ?int $offset = null, ?int $limit = null): void
	{
		if (is_null(AccountRecord::findOne($accountId))) {
			throw new UserException(Strings::ERROR_INVALID_ACCOUNT_ID);
		}

		$query = TransactionHistoryRecord::find()
			->where(['or', ['account_from' => $accountId], ['account_to' => $accountId]])
			->offset($offset)->limit($limit)
			->orderBy(['insert_time' => SORT_DESC]);

		$this->asJson([
			'transactions' => TransactionsPresenter::FormatData($query->all()),
			'meta' => [
				'total' => (int)$query->count(),
				'offset' => ($offset ?? 0),
				'limit' => ((($limit ?? 0) < 0) ? null : $limit)
			]
		]);
	}

	/**
	 * @throws Exception
	 * @throws UserException
	 */
	public function actionTransferFunds(): void
	{
		$model = DynamicModel::validateData(Yii::$app->getRequest()->post(), [
			[['accountFromId', 'accountToId', 'alphabeticCode', 'amount'], 'required'],
			[['accountFromId', 'accountToId'], 'integer', 'min' => 1],
			// TODO: Should change this in models too.
			['accountFromId', 'compare', 'compareAttribute' => 'accountToId', 'operator' => '!=='],
			['alphabeticCode', 'string', 'max' => 3],
			['amount', 'number', 'min' => 0]
		]);
		if (!$model->validate()) {
			throw new UserException(implode(' ', $model->getErrorSummary(false)));
		}

		$accountFrom = AccountRecord::findOne($model->accountFromId);
		if (is_null($accountFrom)) {
			throw new UserException(Strings::ERROR_INVALID_ACCOUNT_ID);
		}

		$accountTo = AccountRecord::findOne($model->accountToId);
		if (is_null($accountTo)) {
			throw new UserException(Strings::ERROR_INVALID_ACCOUNT_ID);
		}

		Yii::$container->get(TransactionService::class)
			->Transfer($accountFrom, $accountTo, $model->alphabeticCode, $model->amount);
	}
}
