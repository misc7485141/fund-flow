<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property string $alphabetic_code
 * @property int $minor_unit
 */
class CurrencyRecord extends ActiveRecord
{
	public static function tableName(): string
	{
		return '{{%currency}}';
	}

	public function rules(): array
	{
		return [
			['alphabetic_code', 'required'],
			['alphabetic_code', 'string', 'max' => 3],
			['minor_unit', 'integer', 'min' => 0]
		];
	}

	public function RoundAmount(float $amount): float
	{
		return round($amount, $this->minor_unit);
	}
}
