<?php

namespace app\models;

use yii\db\{ActiveQuery, ActiveRecord};

/**
 * @property int $id
 * @property int $currency_from
 * @property int $currency_to
 * @property float $rate
 * @property mixed $update_time
 * @property CurrencyRecord $currencyFrom
 * @property CurrencyRecord $currencyTo
 */
class ExchangeRateRecord extends ActiveRecord
{
	const EXPIRES_IN = (60 * 60);
	const DATED_IN = (60 * 10);

	public static function tableName(): string
	{
		return '{{%exchange_rate}}';
	}

	public function rules(): array
	{
		return [
			['currency_from', 'required'],
			['currency_from', 'integer', 'min' => 1],
			['currency_from', 'exist', 'targetClass' => CurrencyRecord::class, 'targetAttribute' => 'id'],
			['currency_to', 'required'],
			['currency_to', 'integer', 'min' => 1],
			['currency_to', 'exist', 'targetClass' => CurrencyRecord::class, 'targetAttribute' => 'id'],
			['rate', 'number', 'min' => 0, 'skipOnEmpty' => true],
			['update_time', 'required'],
			['update_time', 'datetime', 'format' => 'php:Y-m-d H:i:s']
		];
	}

	public function GetCurrencyFrom(): ActiveQuery
	{
		return $this->hasOne(CurrencyRecord::class, ['id' => 'currency_from']);
	}

	public function GetCurrencyTo(): ActiveQuery
	{
		return $this->hasOne(CurrencyRecord::class, ['id' => 'currency_to']);
	}

	public function ConvertFrom(float $amount): float
	{
		return ($amount / $this->rate);
	}

	public function ConvertTo(float $amount): float
	{
		return ($amount * $this->rate);
	}

	public static function UpdateExchangeRate(int $currencyFrom, int $currencyTo, ?float $rate): void
	{
		$record = self::findOne(['currency_from' => $currencyFrom, 'currency_to' => $currencyTo]);
		if (is_null($record)) {
			$record = new self;
			$record->currency_from = $currencyFrom;
			$record->currency_to = $currencyTo;
		}
		$record->rate = $rate;
		$record->update_time = date('Y-m-d H:i:s');
		$record->save();
	}

	public function IsExpired(): bool
	{
		return ((strtotime($this->update_time) + self::EXPIRES_IN) < time());
	}
}
