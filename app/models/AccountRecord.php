<?php

namespace app\models;

use yii\db\{ActiveQuery, ActiveRecord};
use app\resources\Strings;
use OutOfRangeException;

/**
 * @property int $id
 * @property int $client_id
 * @property int $currency_id
 * @property int $balance
 * @property CurrencyRecord $currency
 */
class AccountRecord extends ActiveRecord
{
	public static function tableName(): string
	{
		return '{{%account}}';
	}

	public function rules(): array
	{
		return [
			['client_id', 'required'],
			['client_id', 'integer', 'min' => 1],
			['currency_id', 'required'],
			['currency_id', 'integer', 'min' => 1],
			['currency_id', 'exist', 'targetClass' => CurrencyRecord::class, 'targetAttribute' => 'id'],
			['balance', 'integer', 'min' => 0]
		];
	}

	public function GetCurrency(): ActiveQuery
	{
		return $this->hasOne(CurrencyRecord::class, ['id' => 'currency_id']);
	}

	public function GetPreciseBalance(): float
	{
		return ($this->balance / pow(10, $this->currency->minor_unit));
	}

	public function AddToBalance(float $amount): self
	{
		if ($amount < 0) {
			throw new OutOfRangeException(Strings::ERROR_UNEXPECTED_ARGUMENT);
		}

		$this->balance += round($amount * pow(10, $this->currency->minor_unit));
		return $this;
	}

	public function SubtractFromBalance(float $amount): self
	{
		if ($amount < 0) {
			throw new OutOfRangeException(Strings::ERROR_UNEXPECTED_ARGUMENT);
		}

		$withdraw = round($amount * pow(10, $this->currency->minor_unit));
		if ($this->balance < $withdraw) {
			throw new OutOfRangeException(Strings::ERROR_NOT_ENOUGH_FUNDS_IN_ACCOUNT);
		}

		$this->balance -= $withdraw;
		return $this;
	}
}
