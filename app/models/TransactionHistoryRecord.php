<?php

namespace app\models;

use yii\db\{ActiveQuery, ActiveRecord};

/**
 * @property int $id
 * @property int $account_from
 * @property int $account_to
 * @property string $alphabetic_code_from
 * @property string $alphabetic_code_to
 * @property float $amount_outgoing
 * @property float $amount_incoming
 * @property mixed $insert_time
 * @property AccountRecord $accountFrom
 * @property AccountRecord $accountTo
 */
class TransactionHistoryRecord extends ActiveRecord
{
	public static function tableName(): string
	{
		return '{{%transaction_history}}';
	}

	public function rules(): array
	{
		return [
			['account_from', 'required'],
			['account_from', 'integer', 'min' => 1],
			['account_from', 'exist', 'targetClass' => AccountRecord::class, 'targetAttribute' => 'id'],
			['account_to', 'required'],
			['account_to', 'integer', 'min' => 1],
			['account_to', 'exist', 'targetClass' => AccountRecord::class, 'targetAttribute' => 'id'],
			['alphabetic_code_from', 'required'],
			['alphabetic_code_from', 'string', 'max' => 3],
			['alphabetic_code_to', 'required'],
			['alphabetic_code_to', 'string', 'max' => 3],
			['amount_outgoing', 'required'],
			['amount_outgoing', 'number', 'min' => 0],
			['amount_incoming', 'required'],
			['amount_incoming', 'number', 'min' => 0],
			['insert_time', 'datetime', 'format' => 'php:Y-m-d H:i:s']
		];
	}

	public function GetAccountFrom(): ActiveQuery
	{
		return $this->hasOne(AccountRecord::class, ['id' => 'account_from']);
	}

	public function GetAccountTo(): ActiveQuery
	{
		return $this->hasOne(AccountRecord::class, ['id' => 'account_to']);
	}
}
