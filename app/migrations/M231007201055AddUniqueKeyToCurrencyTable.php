<?php

namespace app\migrations;

use yii\db\Migration;

class M231007201055AddUniqueKeyToCurrencyTable extends Migration
{
	const TABLE_NAME = '{{%currency}}';

	public function safeUp(): void
	{
		$this->createIndex('uq_alphabetic_code', self::TABLE_NAME, 'alphabetic_code', true);
	}

	public function safeDown(): void
	{
		$this->dropIndex('uq_alphabetic_code', self::TABLE_NAME);
	}
}
