<?php

namespace app\migrations;

use yii\db\Migration;

class M231007111535CreateCurrencyTable extends Migration
{
	const TABLE_NAME = '{{%currency}}';

	public function safeUp(): void
	{
		$this->createTable(self::TABLE_NAME, [
			'id' => $this->primaryKey(),
			'alphabetic_code' => $this->string(3)->notNull(),
			'minor_unit' => $this->tinyInteger()->notNull()->defaultValue(0)
		]);
	}

	public function safeDown(): void
	{
		$this->dropTable(self::TABLE_NAME);
	}
}
