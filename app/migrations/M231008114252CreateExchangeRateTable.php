<?php

namespace app\migrations;

use yii\db\Migration;

class M231008114252CreateExchangeRateTable extends Migration
{
	const TABLE_NAME = '{{%exchange_rate}}';

	public function safeUp(): void
	{
		$this->createTable(self::TABLE_NAME, [
			'id' => $this->primaryKey(),
			'currency_from' => $this->integer()->notNull(),
			'currency_to' => $this->integer()->notNull(),
			'rate' => $this->decimal(13, 6)->defaultValue(null),
			'update_time' => $this->timestamp()->notNull()
		]);
		$this->addForeignKey('fk_currency_from', self::TABLE_NAME, 'currency_from', 'currency', 'id',
			'RESTRICT', 'RESTRICT');
		$this->addForeignKey('fk_currency_to', self::TABLE_NAME, 'currency_to', 'currency', 'id',
			'RESTRICT', 'RESTRICT');
		$this->createIndex('uq_currency_from_currency_to', self::TABLE_NAME, ['currency_from', 'currency_to'], true);
	}

	public function safeDown(): void
	{
		$this->dropTable(self::TABLE_NAME);
	}
}
