<?php

namespace app\migrations;

use yii\db\Migration;

class M231007110456CreateAccountTable extends Migration
{
	const TABLE_NAME = '{{%account}}';

	public function safeUp(): void
	{
		$this->createTable(self::TABLE_NAME, [
			'id' => $this->primaryKey(),
			'client_id' => $this->integer()->notNull(),
			'currency_id' => $this->integer()->notNull(),
			'balance' => $this->bigInteger()->notNull()->defaultValue(0)
		]);
	}

	public function safeDown(): void
	{
		$this->dropTable(self::TABLE_NAME);
	}
}
