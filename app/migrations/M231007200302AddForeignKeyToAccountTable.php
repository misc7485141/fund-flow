<?php

namespace app\migrations;

use yii\db\Migration;

class M231007200302AddForeignKeyToAccountTable extends Migration
{
	const TABLE_NAME = '{{%account}}';

	public function safeUp(): void
	{
		$this->addForeignKey('fk_currency_id', self::TABLE_NAME, 'currency_id', 'currency', 'id',
			'RESTRICT', 'RESTRICT');
	}

	public function safeDown(): void
	{
		$this->dropForeignKey('fk_currency_id', self::TABLE_NAME);
	}
}
