<?php

namespace app\migrations;

use yii\db\Migration;

class M231009014730CreateTransactionHistoryTable extends Migration
{
	const TABLE_NAME = '{{%transaction_history}}';

	public function safeUp(): void
	{
		$this->createTable(self::TABLE_NAME, [
			'id' => $this->primaryKey(),
			'account_from' => $this->integer()->notNull(),
			'account_to' => $this->integer()->notNull(),
			'alphabetic_code_from' => $this->string(3)->notNull(),
			'alphabetic_code_to' => $this->string(3)->notNull(),
			'amount_outgoing' => $this->decimal(20, 3)->notNull(),
			'amount_incoming' => $this->decimal(20, 3)->notNull(),
			'insert_time' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP')
		]);
		$this->createIndex('idx_account_from', self::TABLE_NAME, 'account_from');
		$this->createIndex('idx_account_to', self::TABLE_NAME, 'account_to');
	}

	public function safeDown(): void
	{
		$this->dropTable(self::TABLE_NAME);
	}
}
