<?php

namespace app\commands;

use exchangeRates\services\UpdaterService;
use Yii;

/**
 * Console commands related to currency exchange rates.
 */
class ExchangeRatesController extends AppController
{
	const UPDATER_ITERATION_INTERVAL = 15;

	/**
	 * Starts monitoring and updating currency exchange rates. Could be executed by cron through launcher.
	 */
	public function actionLaunchUpdater(): void
	{
		while (true) {
			Yii::$container->get(UpdaterService::class)->Iterate();
			sleep(self::UPDATER_ITERATION_INTERVAL);
		}
	}
}
