<?php

namespace app\commands;

use yii\console\ExitCode;
use Exception, Yii;

/**
 * Console commands that are mainly related to database seeding.
 */
class UtilityController extends AppController
{
	/**
	 * Import currencies into the database from sketchy file I found somewhere on the internet.
	 * @param int|null $maxCount Less currencies means fewer update requests to 3rd party apis.
	 * @throws Exception
	 */
	public function actionImportCurrencies(?int $maxCount = null): int
	{
		$stream = fopen(Yii::getAlias('@misc/currencies.csv'), 'r');
		if ($stream === false) {
			return ExitCode::UNSPECIFIED_ERROR;
		}

		$db = Yii::$app->getDb();
		fgets($stream);
		while (($line = fgetcsv($stream)) !== false) {
			$hasAlphabeticCode = !empty($line[2]);
			$hasMinorUnit = ($line[4] != '-');
			$withdrawn = !empty($line[5]);

			if ($hasAlphabeticCode && $hasMinorUnit && !$withdrawn) {
				if (!is_null($maxCount) && ($maxCount <= 0)) break;
				$result = $db->createCommand('
					INSERT IGNORE INTO {{%currency}} (alphabetic_code, minor_unit)
					VALUES (:alphabeticCode, :minorUnit)
				')->bindValue(':alphabeticCode', $line[2])->bindValue(':minorUnit', $line[4])->execute();

				if ($result) $maxCount--;
				$this->stdout("$line[2] " . ($result ? 'inserted' : 'ignored') . PHP_EOL);
			} else {
				$this->stdout(($hasAlphabeticCode ? $line[2] : 'Unknown') . ' skipped' . PHP_EOL);
			}
		}
		fclose($stream);

		return ExitCode::OK;
	}
}
