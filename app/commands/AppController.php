<?php

namespace app\commands;

use yii\console\Controller;

abstract class AppController extends Controller
{
	/**
	 * @inheritDoc
	 * @return array
	 */
	public function options($actionID): array
	{
		return [];
	}
}
