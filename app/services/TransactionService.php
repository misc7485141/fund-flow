<?php

namespace app\services;

use app\models\{AccountRecord, ExchangeRateRecord, TransactionHistoryRecord};
use yii\base\UserException;
use app\resources\Strings;
use Exception, Yii;

class TransactionService
{
	/**
	 * @throws UserException
	 */
	public function Transfer(AccountRecord $accountFrom, AccountRecord $accountTo,
		string $alphabeticCode, float $amount): void
	{
		if ($accountFrom->id == $accountTo->id) {
			throw new UserException(Strings::ERROR_SAME_ACCOUNT_TRANSFER);
		}

		if ($accountTo->currency->alphabetic_code != $alphabeticCode) {
			throw new UserException(Strings::ERROR_CURRENCY_DESTINATION_ACCOUNT_MISMATCH);
		}

		$exchangeRate = ExchangeRateRecord::findOne([
			'currency_from' => $accountFrom->currency_id,
			'currency_to' => $accountTo->currency_id
		]);
		if (is_null($exchangeRate) || is_null($exchangeRate->rate) || $exchangeRate->IsExpired()) {
			throw new UserException(Strings::ERROR_UNAVAILABLE_OR_EXPIRED_EXCHANGE_RATE);
		}

		$amountRequired = $exchangeRate->ConvertFrom($amount);
		if ($accountFrom->GetPreciseBalance() < $amountRequired) {
			throw new UserException(Strings::ERROR_INSUFFICIENT_FUNDS);
		}

		$transaction = Yii::$app->getDb()->beginTransaction();
		try {
			$accountFrom->SubtractFromBalance($amountRequired)->save();
			$accountTo->AddToBalance($amount)->save();

			$history = new TransactionHistoryRecord;
			$history->account_from = $accountFrom->id;
			$history->account_to = $accountTo->id;
			$history->alphabetic_code_from = $accountFrom->currency->alphabetic_code;
			$history->alphabetic_code_to = $alphabeticCode;
			$history->amount_outgoing = $accountFrom->currency->RoundAmount($amountRequired);
			$history->amount_incoming = $accountTo->currency->RoundAmount($amount);
			$history->save();

			$transaction->commit();
		} catch (Exception) {
			$transaction->rollBack();
			throw new UserException(Strings::ERROR_TRANSACTION_FAILED);
		}
	}
}
