<?php

namespace app\presenters;

use app\models\AccountRecord;

class AccountsPresenter
{
	/**
	 * @param AccountRecord[] $records
	 */
	public static function FormatData(array $records): array
	{
		$result = [];
		foreach ($records as $record) {
			$result[] = [
				'accountId' => $record->id,
				'balance' => "{$record->GetPreciseBalance()} {$record->currency->alphabetic_code}"
			];
		}
		return $result;
	}
}
