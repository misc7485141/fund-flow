<?php

namespace app\presenters;

use app\models\TransactionHistoryRecord;

class TransactionsPresenter
{
	/**
	 * @param TransactionHistoryRecord[] $records
	 */
	public static function FormatData(array $records): array
	{
		$result = [];
		foreach ($records as $record) {
			$result[] = [
				'outgoing' => [
					'accountId' => $record->account_from,
					'amount' => "{$record->accountFrom->currency
						->RoundAmount($record->amount_outgoing)} $record->alphabetic_code_from"
				],
				'incoming' => [
					'accountId' => $record->account_to,
					'amount' => "{$record->accountTo->currency
						->RoundAmount($record->amount_incoming)} $record->alphabetic_code_to"
				],
				'time' => $record->insert_time
			];
		}
		return $result;
	}
}
