<?php

use exchangeRates\providers\ExchangeRatesDataAPI;
use exchangeRates\services\UpdaterService;
use app\services\TransactionService;

Yii::setAlias('@root', dirname(__DIR__, 2));
Yii::setAlias('@app', '@root/app');
Yii::setAlias('@exchangeRates', '@app/modules/exchangeRates');
Yii::setAlias('@misc', '@root/misc');
Yii::setAlias('@vendor', '@root/vendor');

Yii::$container->setSingletons([
	UpdaterService::class => fn(): UpdaterService => new UpdaterService([
		new ExchangeRatesDataAPI
	]),
	TransactionService::class => fn(): TransactionService => new TransactionService
]);
