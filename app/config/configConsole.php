<?php

use exchangeRates\Bootable as ExchangeRatesBootable;
use yii\console\controllers\MigrateController;
use yii\db\Connection as DbConnection;
use yii\debug\Module as DebugModule;
use yii\log\FileTarget;
use yii\base\Event;

$config = [
	'id' => 'console',
	'basePath' => '@app',
	'vendorPath' => '@vendor',
	'enableCoreCommands' => false,
	'timeZone' => 'UTC',
	'controllerNamespace' => 'app\commands',
	'controllerMap' => [
		'migrate' => [
			'class' => MigrateController::class,
			'migrationPath' => null,
			'migrationNamespaces' => [
				'app\migrations'
			]
		]
	],
	'components' => [
		'db' => [
			'class' => DbConnection::class,
			'dsn' => 'mysql:host=localhost',
			'username' => 'root',
			'password' => '',
			'on afterOpen' => function (Event $event): void {
				/** @var DbConnection $db */
				$db = $event->sender;
				$db->createCommand('SET time_zone = \'UTC\'')->execute();
				$db->createCommand('CREATE DATABASE IF NOT EXISTS homework')->execute();
				$db->createCommand('USE homework')->execute();
			}
		],
		'log' => [
			'targets' => [
				[
					'class' => FileTarget::class,
					'logFile' => '@runtime/logs/console.log',
					'levels' => [
						'error',
						'warning'
					]
				]
			]
		]
	],
	'modules' => [
		'exchangeRates' => [
			'class' => ExchangeRatesBootable::class
		]
	],
	'bootstrap' => [
		'log'
	],
	'params' => (require __DIR__ . '/params.php')
];

if (YII_DEBUG) {
	$config['modules']['debug'] = [
		'class' => DebugModule::class
	];
	$config['bootstrap'][] = 'debug';
}

return $config;
