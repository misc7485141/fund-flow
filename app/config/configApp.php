<?php

use exchangeRates\Bootable as ExchangeRatesBootable;
use yii\db\Connection as DbConnection;
use yii\debug\Module as DebugModule;
use app\components\JsonErrorHandler;
use yii\log\FileTarget;
use yii\base\Event;

$config = [
	'id' => 'app',
	'basePath' => '@app',
	'vendorPath' => '@vendor',
	'timeZone' => 'UTC',
	'controllerNamespace' => 'app\controllers',
	'components' => [
		'errorHandler' => [
			'class' => JsonErrorHandler::class
		],
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false
		],
		'db' => [
			'class' => DbConnection::class,
			'dsn' => 'mysql:host=localhost;dbname=homework',
			'username' => 'root',
			'password' => '',
			'on afterOpen' => function (Event $event): void {
				/** @var DbConnection $db */
				$db = $event->sender;
				$db->createCommand('SET time_zone = \'UTC\'')->execute();
			}
		],
		'log' => [
			'targets' => [
				[
					'class' => FileTarget::class,
					'logFile' => '@runtime/logs/app.log',
					'levels' => [
						'error',
						'warning'
					]
				]
			]
		]
	],
	'modules' => [
		'exchangeRates' => [
			'class' => ExchangeRatesBootable::class
		]
	],
	'bootstrap' => [
		'log'
	],
	'params' => (require __DIR__ . '/params.php')
];

if (YII_DEBUG) {
	$config['modules']['debug'] = [
		'class' => DebugModule::class
	];
	$config['bootstrap'][] = 'debug';
}

return $config;
