<?php

declare(strict_types=1);

namespace app\tests\support;

use app\tests\support\_generated\UnitTesterActions;
use Codeception\Actor;

class UnitTester extends Actor
{
	use UnitTesterActions;
}
