<?php

namespace app\tests\support\fixtures;

use app\models\TransactionHistoryRecord;
use yii\test\ActiveFixture;

class TransactionHistoryRecordFixture extends ActiveFixture
{
	/** @var string */
	public $modelClass = TransactionHistoryRecord::class;
	/** @var string[] */
	public $depends = [
		AccountRecordFixture::class
	];
}
