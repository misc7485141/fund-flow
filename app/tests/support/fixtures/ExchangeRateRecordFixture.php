<?php

namespace app\tests\support\fixtures;

use app\models\ExchangeRateRecord;
use yii\test\ActiveFixture;

class ExchangeRateRecordFixture extends ActiveFixture
{
	/** @var string */
	public $modelClass = ExchangeRateRecord::class;
	/** @var string[] */
	public $depends = [
		CurrencyRecordFixture::class
	];
}
