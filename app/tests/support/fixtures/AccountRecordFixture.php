<?php

namespace app\tests\support\fixtures;

use app\models\AccountRecord;
use yii\test\ActiveFixture;

class AccountRecordFixture extends ActiveFixture
{
	/** @var string */
	public $modelClass = AccountRecord::class;
	/** @var string[] */
	public $depends = [
		CurrencyRecordFixture::class
	];
}
