<?php

namespace app\tests\support\fixtures;

use app\models\CurrencyRecord;
use yii\test\ActiveFixture;

class CurrencyRecordFixture extends ActiveFixture
{
	/** @var string */
	public $modelClass = CurrencyRecord::class;
}
