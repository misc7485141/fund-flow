<?php

namespace app\tests\unit\models;

use app\tests\support\fixtures\{CurrencyRecordFixture, ExchangeRateRecordFixture};
use Codeception\Exception\ModuleException;
use app\models\ExchangeRateRecord;
use app\tests\support\UnitTester;
use Codeception\Test\Unit;

class ExchangeRateRecordTest extends Unit
{
	protected UnitTester $tester;

	public function _fixtures(): array
	{
		return [
			'exchangeRateRecords' => [
				'class' => ExchangeRateRecordFixture::class,
				'dataFile' => codecept_data_dir() . 'exchangeRateRecords.php'
			],
			'currencyRecords' => [
				'class' => CurrencyRecordFixture::class,
				'dataFile' => codecept_data_dir() . 'currencyRecords.php'
			]
		];
	}

	/**
	 * @throws ModuleException
	 */
	public function testConvertFrom(): void
	{
		$this->assertEquals(5, $this->tester->grabFixture('exchangeRateRecords', 'exchangeRate4')->ConvertFrom(5));
		$this->assertEquals(33.459249, round($this->tester->grabFixture('exchangeRateRecords', 'exchangeRate2')
			->ConvertFrom(25.687), 6));
		$this->assertEquals(1.402098, round($this->tester->grabFixture('exchangeRateRecords', 'exchangeRate1')
			->ConvertFrom(113), 6));
	}

	/**
	 * @throws ModuleException
	 */
	public function testConvertTo(): void
	{
		$this->assertEquals(5, $this->tester->grabFixture('exchangeRateRecords', 'exchangeRate4')->ConvertTo(5));
		$this->assertEquals(19.720167, round($this->tester->grabFixture('exchangeRateRecords', 'exchangeRate2')
			->ConvertTo(25.687), 6));
		$this->assertEquals(9107.06663, round($this->tester->grabFixture('exchangeRateRecords', 'exchangeRate1')
			->ConvertTo(113), 6));
	}

	public function testUpdateExchangeRate(): void
	{
		$currencyFrom = $currencyTo = $rate = 1;
		$condition = ['currency_from' => $currencyFrom, 'currency_to' => $currencyTo];

		ExchangeRateRecord::deleteAll($condition);
		ExchangeRateRecord::UpdateExchangeRate($currencyFrom, $currencyTo, $rate);
		$newRecord = ExchangeRateRecord::findOne($condition);
		$this->assertNotNull($newRecord);
		$this->assertEquals($newRecord->currency_from, $currencyFrom);
		$this->assertEquals($newRecord->currency_to, $currencyTo);
		$this->assertEquals($newRecord->rate, $rate);

		sleep(1);

		ExchangeRateRecord::UpdateExchangeRate($currencyFrom, $currencyTo, null);
		$allRecords = ExchangeRateRecord::findAll($condition);
		$this->assertCount(1, $allRecords);
		$updatedRecord = reset($allRecords);
		$this->assertEquals($updatedRecord->currency_from, $newRecord->currency_from);
		$this->assertEquals($updatedRecord->currency_to, $newRecord->currency_to);
		$this->assertNull($updatedRecord->rate);
		$this->assertLessThan($updatedRecord->update_time, $newRecord->update_time);
	}

	/**
	 * @throws ModuleException
	 */
	public function testIsExpired(): void
	{
		$this->assertTrue($this->tester->grabFixture('exchangeRateRecords', 'exchangeRate3')->IsExpired());
		$this->assertFalse($this->tester->grabFixture('exchangeRateRecords', 'exchangeRate1')->IsExpired());
	}
}
