<?php

namespace app\tests\unit\models;

use app\tests\support\fixtures\CurrencyRecordFixture;
use Codeception\Exception\ModuleException;
use app\tests\support\UnitTester;
use Codeception\Test\Unit;

class CurrencyRecordTest extends Unit
{
	protected UnitTester $tester;

	public function _fixtures(): array
	{
		return [
			'currencyRecords' => [
				'class' => CurrencyRecordFixture::class,
				'dataFile' => codecept_data_dir() . 'currencyRecords.php'
			]
		];
	}

	/**
	 * @throws ModuleException
	 */
	public function testRoundAmount(): void
	{
		$this->assertEquals(10, $this->tester->grabFixture('currencyRecords', 'currency4')->RoundAmount(10));
		$this->assertEquals(0, $this->tester->grabFixture('currencyRecords', 'currency5')->RoundAmount(0));
		$this->assertEquals(10.56, $this->tester->grabFixture('currencyRecords', 'currency2')->RoundAmount(10.5647));
	}
}
