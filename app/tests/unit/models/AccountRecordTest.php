<?php

namespace app\tests\unit\models;

use app\tests\support\fixtures\{AccountRecordFixture, CurrencyRecordFixture};
use Codeception\Exception\ModuleException;
use app\tests\support\UnitTester;
use Codeception\Test\Unit;
use OutOfRangeException;

class AccountRecordTest extends Unit
{
	protected UnitTester $tester;

	public function _fixtures(): array
	{
		return [
			'accountRecords' => [
				'class' => AccountRecordFixture::class,
				'dataFile' => codecept_data_dir() . 'accountRecords.php'
			],
			'currencyRecords' => [
				'class' => CurrencyRecordFixture::class,
				'dataFile' => codecept_data_dir() . 'currencyRecords.php'
			]
		];
	}

	/**
	 * @throws ModuleException
	 */
	public function testGetPreciseBalance(): void
	{
		$this->assertEquals(24.52, $this->tester->grabFixture('accountRecords', 'account1')->GetPreciseBalance());
		$this->assertEquals(0, $this->tester->grabFixture('accountRecords', 'account3')->GetPreciseBalance());
		$this->assertEquals(10, $this->tester->grabFixture('accountRecords', 'account4')->GetPreciseBalance());
	}

	/**
	 * @throws ModuleException
	 */
	public function testAddToBalance(): void
	{
		$this->assertEquals(5496, $this->tester->grabFixture('accountRecords', 'account3')
			->AddToBalance(5.496)->balance);
		$this->assertEquals(12, $this->tester->grabFixture('accountRecords', 'account4')
			->AddToBalance(2)->balance);
		$this->assertEquals(2452, $this->tester->grabFixture('accountRecords', 'account1')
			->AddToBalance(0)->balance);
		$this->assertEquals(6879, $this->tester->grabFixture('accountRecords', 'account2')
			->AddToBalance(2.45677)->balance);
	}

	/**
	 * @throws ModuleException
	 */
	public function testAddToBalanceNegativeAmount(): void
	{
		$this->expectException(OutOfRangeException::class);
		$this->tester->grabFixture('accountRecords', 'account4')->AddToBalance(-2);
	}

	/**
	 * @throws ModuleException
	 */
	public function testSubtractFromBalance(): void
	{
		$this->assertEquals(8, $this->tester->grabFixture('accountRecords', 'account4')
			->SubtractFromBalance(2)->balance);
		$this->assertEquals(2000, $this->tester->grabFixture('accountRecords', 'account1')
			->SubtractFromBalance(4.52)->balance);
		$this->assertEquals(6387, $this->tester->grabFixture('accountRecords', 'account2')
			->SubtractFromBalance(2.45677)->balance);
	}

	/**
	 * @throws ModuleException
	 */
	public function testSubtractFromBalanceNegativeAmount(): void
	{
		$this->expectException(OutOfRangeException::class);
		$this->tester->grabFixture('accountRecords', 'account3')->SubtractFromBalance(-1);
	}

	/**
	 * @throws ModuleException
	 */
	public function testSubtractFromBalanceTooMuch(): void
	{
		$this->expectException(OutOfRangeException::class);
		$this->tester->grabFixture('accountRecords', 'account3')->SubtractFromBalance(1);
	}
}
