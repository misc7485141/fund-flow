<?php

namespace app\tests\unit\models;

use app\tests\support\fixtures\{AccountRecordFixture, CurrencyRecordFixture, ExchangeRateRecordFixture};
use yii\base\{InvalidConfigException, UserException};
use Codeception\Exception\ModuleException;
use app\models\TransactionHistoryRecord;
use yii\di\NotInstantiableException;
use app\services\TransactionService;
use app\tests\support\UnitTester;
use app\resources\Strings;
use Codeception\Test\Unit;
use Yii;

class TransactionServiceTest extends Unit
{
	protected UnitTester $tester;

	public function _fixtures(): array
	{
		return [
			'accountRecords' => [
				'class' => AccountRecordFixture::class,
				'dataFile' => codecept_data_dir() . 'accountRecords.php'
			],
			'currencyRecords' => [
				'class' => CurrencyRecordFixture::class,
				'dataFile' => codecept_data_dir() . 'currencyRecords.php'
			],
			'exchangeRateRecords' => [
				'class' => ExchangeRateRecordFixture::class,
				'dataFile' => codecept_data_dir() . 'exchangeRateRecords.php'
			]
		];
	}

	/**
	 * @throws InvalidConfigException
	 * @throws ModuleException
	 * @throws NotInstantiableException
	 * @throws UserException
	 */
	public function testTransfer(): void
	{
		$accountFrom = $this->tester->grabFixture('accountRecords', 'account1');
		$accountTo = $this->tester->grabFixture('accountRecords', 'account2');
		Yii::$container->get(TransactionService::class)->Transfer($accountFrom, $accountTo, 'USD', 5.99);
		$last = TransactionHistoryRecord::find()->orderBy(['id' => SORT_DESC])->one();
		$this->assertNotNull($last);
		$this->assertEquals($last->account_from, $accountFrom->id);
		$this->assertEquals($last->account_to, $accountTo->id);
		$this->assertEquals($last->alphabetic_code_from, $accountFrom->currency->alphabetic_code);
		$this->assertEquals($last->alphabetic_code_to, 'USD');
		$this->assertEquals($last->amount_outgoing, 0.07);
		$this->assertEquals($last->amount_incoming, 5.99);
	}

	/**
	 * @throws InvalidConfigException
	 * @throws ModuleException
	 * @throws NotInstantiableException
	 * @throws UserException
	 */
	public function testTransferSameAccount(): void
	{
		$accountFrom = $accountTo = $this->tester->grabFixture('accountRecords', 'account1');
		$this->expectException(UserException::class);
		$this->expectExceptionMessage(Strings::ERROR_SAME_ACCOUNT_TRANSFER);
		Yii::$container->get(TransactionService::class)->Transfer($accountFrom, $accountTo, 'USD', 5.99);
	}

	/**
	 * @throws InvalidConfigException
	 * @throws ModuleException
	 * @throws NotInstantiableException
	 * @throws UserException
	 */
	public function testTransferCurrencyMismatch(): void
	{
		$accountFrom = $this->tester->grabFixture('accountRecords', 'account1');
		$accountTo = $this->tester->grabFixture('accountRecords', 'account2');
		$this->expectException(UserException::class);
		$this->expectExceptionMessage(Strings::ERROR_CURRENCY_DESTINATION_ACCOUNT_MISMATCH);
		Yii::$container->get(TransactionService::class)->Transfer($accountFrom, $accountTo, 'LVL', 5.99);
	}

	/**
	 * @throws InvalidConfigException
	 * @throws ModuleException
	 * @throws NotInstantiableException
	 * @throws UserException
	 */
	public function testTransferUnavailableExchangeRate(): void
	{
		$accountFrom = $this->tester->grabFixture('accountRecords', 'account2');
		$accountTo = $this->tester->grabFixture('accountRecords', 'account1');
		$this->expectException(UserException::class);
		$this->expectExceptionMessage(Strings::ERROR_UNAVAILABLE_OR_EXPIRED_EXCHANGE_RATE);
		Yii::$container->get(TransactionService::class)->Transfer($accountFrom, $accountTo, 'EUR', 5.99);
	}

	/**
	 * @throws InvalidConfigException
	 * @throws ModuleException
	 * @throws NotInstantiableException
	 * @throws UserException
	 */
	public function testTransferExpiredExchangeRate(): void
	{
		$accountFrom = $this->tester->grabFixture('accountRecords', 'account1');
		$accountTo = $this->tester->grabFixture('accountRecords', 'account4');
		$this->expectException(UserException::class);
		$this->expectExceptionMessage(Strings::ERROR_UNAVAILABLE_OR_EXPIRED_EXCHANGE_RATE);
		Yii::$container->get(TransactionService::class)->Transfer($accountFrom, $accountTo, 'XPF', 5.99);
	}

	/**
	 * @throws InvalidConfigException
	 * @throws ModuleException
	 * @throws NotInstantiableException
	 * @throws UserException
	 */
	public function testTransferInsufficientFunds(): void
	{
		$accountFrom = $this->tester->grabFixture('accountRecords', 'account1');
		$accountTo = $this->tester->grabFixture('accountRecords', 'account2');
		$this->expectException(UserException::class);
		$this->expectExceptionMessage(Strings::ERROR_INSUFFICIENT_FUNDS);
		Yii::$container->get(TransactionService::class)->Transfer($accountFrom, $accountTo, 'USD', 1976.155);
	}
}
