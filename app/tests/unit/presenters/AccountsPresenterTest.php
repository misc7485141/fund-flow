<?php

namespace app\tests\unit\models;

use app\tests\support\fixtures\{AccountRecordFixture, CurrencyRecordFixture};
use Codeception\Exception\ModuleException;
use app\presenters\AccountsPresenter;
use app\tests\support\UnitTester;
use Codeception\Test\Unit;

class AccountsPresenterTest extends Unit
{
	protected UnitTester $tester;

	public function _fixtures(): array
	{
		return [
			'accountRecords' => [
				'class' => AccountRecordFixture::class,
				'dataFile' => codecept_data_dir() . 'accountRecords.php'
			],
			'currencyRecords' => [
				'class' => CurrencyRecordFixture::class,
				'dataFile' => codecept_data_dir() . 'currencyRecords.php'
			]
		];
	}

	/**
	 * @throws ModuleException
	 */
	public function testFormatData(): void
	{
		$this->assertEquals([
			[
				'accountId' => 1,
				'balance' => '24.52 EUR'
			],
			[
				'accountId' => 2,
				'balance' => '66.33 USD'
			]
		], AccountsPresenter::FormatData([
			$this->tester->grabFixture('accountRecords', 'account1'),
			$this->tester->grabFixture('accountRecords', 'account2')
		]));
	}
}
