<?php

namespace app\tests\unit\models;

use app\tests\support\fixtures\{AccountRecordFixture, CurrencyRecordFixture, TransactionHistoryRecordFixture};
use Codeception\Exception\ModuleException;
use app\presenters\TransactionsPresenter;
use app\tests\support\UnitTester;
use Codeception\Test\Unit;

class TransactionsPresenterTest extends Unit
{
	protected UnitTester $tester;

	public function _fixtures(): array
	{
		return [
			'transactionHistoryRecords' => [
				'class' => TransactionHistoryRecordFixture::class,
				'dataFile' => codecept_data_dir() . 'transactionHistoryRecords.php'
			],
			'accountRecords' => [
				'class' => AccountRecordFixture::class,
				'dataFile' => codecept_data_dir() . 'accountRecords.php'
			],
			'currencyRecords' => [
				'class' => CurrencyRecordFixture::class,
				'dataFile' => codecept_data_dir() . 'currencyRecords.php'
			]
		];
	}

	/**
	 * @throws ModuleException
	 */
	public function testFormatData(): void
	{
		$this->assertEquals([
			[
				'outgoing' => [
					'accountId' => 1,
					'amount' => '20.47 EUR'
				],
				'incoming' => [
					'accountId' => 2,
					'amount' => '21.58 USD'
				],
				'time' => '2022-09-12 21:07:56'
			],
			[
				'outgoing' => [
					'accountId' => 3,
					'amount' => '2 JOD'
				],
				'incoming' => [
					'accountId' => 4,
					'amount' => '1754 XPF'
				],
				'time' => '2023-10-10 13:00:14'
			]
		], TransactionsPresenter::FormatData([
			$this->tester->grabFixture('transactionHistoryRecords', 'transactionHistory1'),
			$this->tester->grabFixture('transactionHistoryRecords', 'transactionHistory2')
		]));
	}
}
