<?php

return [
	'currency1' => [
		'id' => 1,
		'alphabetic_code' => 'EUR',
		'minor_unit' => 2
	],
	'currency2' => [
		'id' => 2,
		'alphabetic_code' => 'USD',
		'minor_unit' => 2
	],
	'currency3' => [
		'id' => 3,
		'alphabetic_code' => 'JOD',
		'minor_unit' => 3
	],
	'currency4' => [
		'id' => 4,
		'alphabetic_code' => 'XPF',
		'minor_unit' => 0
	],
	'currency5' => [
		'id' => 5,
		'alphabetic_code' => 'CLF',
		'minor_unit' => 4
	]
];
