<?php

return [
	'account1' => [
		'id' => 1,
		'client_id' => 8,
		'currency_id' => 1,
		'balance' => 2452
	],
	'account2' => [
		'id' => 2,
		'client_id' => 8,
		'currency_id' => 2,
		'balance' => 6633
	],
	'account3' => [
		'id' => 3,
		'client_id' => 8,
		'currency_id' => 3,
		'balance' => 0
	],
	'account4' => [
		'id' => 4,
		'client_id' => 8,
		'currency_id' => 4,
		'balance' => 10
	]
];
