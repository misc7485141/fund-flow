<?php

return [
	'transactionHistory1' => [
		'id' => 1,
		'account_from' => 1,
		'account_to' => 2,
		'alphabetic_code_from' => 'EUR',
		'alphabetic_code_to' => 'USD',
		'amount_outgoing' => 20.47,
		'amount_incoming' => 21.58,
		'insert_time' => '2022-09-12 21:07:56'
	],
	'transactionHistory2' => [
		'id' => 2,
		'account_from' => 3,
		'account_to' => 4,
		'alphabetic_code_from' => 'JOD',
		'alphabetic_code_to' => 'XPF',
		'amount_outgoing' => 2,
		'amount_incoming' => 1754,
		'insert_time' => '2023-10-10 13:00:14'
	]
];
