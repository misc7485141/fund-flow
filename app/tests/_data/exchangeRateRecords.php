<?php

return [
	'exchangeRate1' => [
		'id' => 1,
		'currency_from' => 1,
		'currency_to' => 2,
		'rate' => 80.593510,
		'update_time' => date('Y-m-d H:i:s')
	],
	'exchangeRate2' => [
		'id' => 2,
		'currency_from' => 4,
		'currency_to' => 3,
		'rate' => 0.767710,
		'update_time' => date('Y-m-d H:i:s')
	],
	'exchangeRate3' => [
		'id' => 3,
		'currency_from' => 1,
		'currency_to' => 4,
		'rate' => 1,
		'update_time' => '2023-10-12 21:00:14'
	],
	'exchangeRate4' => [
		'id' => 4,
		'currency_from' => 2,
		'currency_to' => 2,
		'rate' => 1,
		'update_time' => date('Y-m-d H:i:s')
	]
];
